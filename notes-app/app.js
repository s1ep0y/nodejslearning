const chalk = require ('chalk')
const yargs = require ('yargs')
const notes = require('./src/notes.js')
const fs = require ('fs')

yargs.command({
    command: 'add',
    describe: 'Add a new note',
    builder: {
        title: {
            describe: 'Title for note',
            demandOption: true,
            type: 'string'
        },
        body:{
            describe: 'Body for this note',
            demandOption: true,
            type: 'string'
        }
    },
    handler (argv){
        notes.addNote(argv.title, argv.body)
    }
})

yargs.command({
    command: 'remove',
    describe: 'Remove note',
    builder: {
        title: 
        {
        describe: 'Title for note',
        demandOption: true,
        type: 'string'
        },
    },
    handler (argv){
        notes.removeNote(argv.title)
    }
})

yargs.command({
    command: 'list',
    describe: 'Listing your notes',
    handler (){
        notes.listNote()
    }
})

yargs.command({
    command: 'read',
    describe: 'Read note',
    builder: {
        title: {
            describe: 'Title for note',
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv){
        notes.readNote(argv.title)
    }
})

yargs.parse()