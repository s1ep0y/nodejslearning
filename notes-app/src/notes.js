const fs = require ('fs')
const chalk = require('chalk')

const addNote = (title, body) => {
    var notes = loadNotes()
    var duplNote = notes.find((note) => note.title === title)
    if(!duplNote){
        notes.push({
            title,
            body
        })        
        saveNotes(notes)
        console.log(chalk.green.inverse(`Note with title "${title}" added`))
    } else{
        console.log(chalk.red.inverse('Note title taken'))
    }
    
}

const removeNote = (title) =>{
    var notes = loadNotes()
    var duplNotes = notes.filter(((note) => note.title !== title))
    if (notes.length > duplNotes.length){
        console.log(chalk.green.inverse(`"${title}" note has been removed`))
        saveNotes(duplNotes)
    } else{
        console.log(chalk.red.inverse(`Note "${title}" not found`))
    }
    
    
}

const readNote = (title) =>{
    var notes = loadNotes()
    var duplNote = notes.find((note) => note.title === title)
    if (duplNote){
        console.log(chalk.green.inverse(`"${duplNote.title}" note has been found`))
        console.log('_______')
        console.log(duplNote.body)
    } else{
        console.log(chalk.red.inverse(`Note "${title}" not found`))
    }
}

const listNote = () =>{
    var notes = loadNotes()
    console.log(chalk.underline('Your notes'))
    notes.forEach((note) => {
        console.log(chalk.cyan(`- ${note.title}`))
    });
}

const saveNotes = (notes) =>{
    fs.writeFileSync('notes.json', JSON.stringify(notes))
}

const loadNotes = () =>{
    try {
        return JSON.parse(fs.readFileSync('notes.json').toString())
    } catch(e){
        return []
    }
}

module.exports = {
    removeNote,
    listNote,
    readNote,
    addNote}